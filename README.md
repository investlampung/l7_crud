## Belajar CRUD Di Laravel 7

Langkah - langkah yang dilakukan setelah create project.
## Set Autentifikasi
  * buat database dulu dan set .env
  * php artisan migrate
  * composer require laravel/ui (untuk download dependence, dependence adalah tool untuk membantu dalam coding)
  * php artisan ui vue --auth (untuk men generate package dan route untuk autentifikasi)
  * npm install && npm run dev (kalo langkah disamping error lakukan 1 1 , npm install baru npm run dev)

## Set Multiple Role (Jika user bermacam" : admin,guru,siswa dll)
  * buat field role di table user
  * php artisan make:middleware Role (membuat Middleware Role)
  * edit file Providers->AuthServiceProfider.php
  * edit file Kernel.php : bagian routeMiddleware
  * tambahkan __construct di Setiap Controller

## Laravel Mail
  * Daftar MailGun
  * set .env mail smtp di isi dengan smtp di mailgun(ada di menu sending overview)
  * php artisan make:mail HelloMail